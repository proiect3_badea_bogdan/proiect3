from django.shortcuts import render
import requests
import json
import pandas as pd

url = "https://covid-193.p.rapidapi.com/statistics"

headers = {
    'x-rapidapi-host': "covid-193.p.rapidapi.com",
    'x-rapidapi-key': "a11dae859emsh31b8c4ffe5d7048p1a0e94jsn8c6095f641b4"
    }

response = requests.request("GET", url, headers=headers).json()



def cases_per_country_view(request):
    nr_results = int(response['results'])
    mylist = []
    for x in range(0,nr_results):
        mylist.append(response['response'][x]['country'])
    mylist = sorted(mylist)

    if request.method == "POST":

        start_date = request.POST['trip-start']
        end_date = request.POST['trip-end']
        lista_pandas = pd.date_range(start_date, end_date, freq='D')
        L = []
        for time in pd.date_range(start_date, end_date, freq='5Min'):    
            L.append(pd.date_range(time, freq='S', periods=1).strftime("%Y-%m-%d").tolist())
        p = []
        perioada = []
        [p.append(x) for x in L if x not in p]
        for elem in p:
        	perioada.append(elem[0])

        selected_country = request.POST['selected_country']
        nr_results = int(response['results'])
        for x in range(0,nr_results):
            if selected_country == response['response'][x]['country']:
                new = response['response'][x]['cases']['new']
                active = response['response'][x]['cases']['active']
                critical = response['response'][x]['cases']['critical']
                recovered = response['response'][x]['cases']['recovered']
                total = response['response'][x]['cases']['total']
                deaths = int(total) - int(active) - int(recovered)

        url = "https://covid-193.p.rapidapi.com/history"
        querystrings = []
        responses = []
        months = ['01','02','03','04','05','06','07','08','09','10','11','12']
        days = []
        active_cases = []

        for date in perioada:
            string = {"country": selected_country, "day": date}
            querystrings.append(string)	

        headers = {
            'x-rapidapi-host': "covid-193.p.rapidapi.com",
            'x-rapidapi-key': "a11dae859emsh31b8c4ffe5d7048p1a0e94jsn8c6095f641b4"
            }

        for querystring in querystrings:
            response2 = requests.request("GET", url, headers=headers, params=querystring).json()
            try:
                active_cases.append(response2['response'][0]['cases']['active'])
                days.append(response2['response'][0]['day'])
            except:
                pass
        context = {'active_cases':active_cases, 'days': days, 'selected_country': selected_country, 'mylist': mylist, 'new': new, 'active':active, 'critical':critical, 'recovered':recovered, 'deaths':deaths, 'total':total}
        return render(request, 'cases_per_country.html',context)


    nr_results = int(response['results'])
    mylist = []
    for x in range(0,nr_results):
        mylist.append(response['response'][x]['country'])
    mylist = sorted(mylist)
    context = {'mylist':mylist}
    return render(request, 'cases_per_country.html',context)
