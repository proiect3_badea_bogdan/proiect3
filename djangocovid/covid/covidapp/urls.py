from django.contrib import admin
from django.urls import path
from .views import cases_per_country_view

urlpatterns = [
    path('',cases_per_country_view)
]
